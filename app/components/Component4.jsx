var React = require('react');
var ReactDOM = require('react-dom');
var Component4 = React.createClass({
  getInitialState: function(){
    return {
      message: "",
      rank : 3
    };
  },
  handleStatusChange: function(newStatus) {
    this.setState({
      message: newStatus
    });
      this.props.onStatusChange(newStatus,3);

  },
  render: function() {
    return (
      <div className = "component4">Component 4
        <span> {this.state.message}</span>
        <input type ="button" onClick = {() => { this.handleStatusChange('This is Component 4')}} value = "click 4"/>
      </div>
    );
  }


});

module.exports = Component4;
