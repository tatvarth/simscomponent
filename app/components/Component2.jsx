var React = require('react');
var ReactDOM = require('react-dom');
var Component3 = require('Component3');
var Component4 = require('Component4');

var Component2 = React.createClass({
  getInitialState: function(){
    return {
      message: "",
      rank: 2
    };
  },
  handleStatusChange: function(newStatus,rank) {
      if(this.state.rank <= rank){
      this.setState({
        message: newStatus
      });
      this.props.onStatusChange(newStatus,rank);
    }

  },
  render: function() {
    return (
      <div className="component2">Component 2
        <span> {this.state.message}</span>
        <input type ="button" onClick = {() => { this.handleStatusChange('This is Component 2',2)}} value="Click 2"></input>
        <div className = "holder34">
          <Component3 onStatusChange = {this.handleStatusChange} />
          <Component4 onStatusChange = {this.handleStatusChange}/>
        </div>

      </div>
    );
  }


});

module.exports = Component2;
