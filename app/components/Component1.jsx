var React = require('react');
var ReactDOM = require('react-dom');
var Component2 = require('Component2');

var Component1 = React.createClass({
  getInitialState: function(){
    return {
      message: "",
      rank: 1,
      summary: []
    };
  },
  handleStatusChange: function(newStatus,rank) {
    if(this.state.rank <= rank){
      this.setState({
        message: newStatus,
        rank: rank,
        summary: [...this.state.summary, newStatus]
      });
    }

  },
  handleSummaryClick: function(newStatus) {
    document.getElementById('summarySection').innerHTML = this.state.summary.join(' , ');
  },
  render: function() {
    return (
      <div className="component1">Component 1
        <span> {this.state.message}</span>
        <input type ="button" value = "Click 1" onClick = {() => { this.handleStatusChange('This is Component 1',1)}} />
        <Component2 onStatusChange = {this.handleStatusChange}/>
        <input type="button" id = "summary"  onClick = {() => { this.handleSummaryClick('Component 1')}} value="Summary"/>
        <div id="summarySection"> </div>
      </div>
    );
  }


});

module.exports = Component1;
