var React = require('react');
var ReactDOM = require('react-dom');
var Component3 = React.createClass({
  getInitialState: function(){
    return {
      message: "",
      rank: 3
    };
  },
  handleStatusChange: function(newStatus) {
      this.setState({
        message: newStatus
      });
      this.props.onStatusChange(newStatus,3);

  },
  render: function() {
    return (
      <div className="component3">Component 3
        <span> {this.state.message}</span>
        <input type ="button" onClick = {() => { this.handleStatusChange('This is Component 3')}} value = "click 3"/>
      </div>
    );
  }


});

module.exports = Component3;
